$(window).load(function() {
	if ( window.innerWidth < 1400 ) {
		datafix(0);		
	};

	if ( window.screen.width <= 700 ) {
		$('meta[name=viewport]').attr('content', 'width=device-width,  initial-scale=0.3, maximum-scale=1.5');
	
	};


	var s = skrollr.init({
		mobileDeceleration:1,
		constants: {
	        turn: 1300,
	        turnend: 1900,
	    }
	});	


	$(window).resize(function(){
		if ( window.innerWidth < 1400 ) {
			datafix(0);	
		}
		else{
			datafix(1);		
		};
		s.refresh();
	});
	
	
	function datafix(f){
		if (f==0) {
			$('#level').attr('data-4500','top:-575px;');

		}
		else{
			$('#level').attr('data-4500','top:-800px;');

		};
		
	};

	// $(window).scroll(function () {
	// 	var top=$(window).scrollTop();
	// 	console.log(top);		

	// });	
	
	//video pop event
	$('.video').click(function(){
		$.fancybox.open({
			href : 'iframe.html',
			type : 'iframe',
			padding : 5,
			width: '90%',
			height: '90%',
			minHeight: '480px'
		});
	});

	//slider1 click
	var li = $('#slider .ctrl li'),
		slider1 = $('#slider .inner');
	
	li.click(function(){
		var act= $('#slider .ctrl li.act').index(),
			current = $(this).index();	
		if ( current !==act ) {
			slider1_move( current );
		};		
	});

	$('#slider').mouseenter(function(){
		$('#slider .pre').css({display:'block'});
		$('#slider .next').css({display:'block'});
	});
	$('#slider').mouseleave(function(){
		$('#slider .pre').css({display:'none'});
		$('#slider .next').css({display:'none'});
	});


	$('#slider .pre').click(function(){
		var act = $('#slider .ctrl li.act').index();	
		if (act==0) {
			slider1_move(2);
		}
		else{
			slider1_move(act-1);
		};
	});
	$('#slider .next').click(function(){
		var act = $('#slider .ctrl li.act').index();	
		if (act==2) {
			slider1_move(0);
		}
		else{
			slider1_move(act+1);
		};
	});

	
	function slider1_move(i){
		var next = $('#slider .ctrl li').eq(i);

		slider1.fadeOut(300, function(){
			slider1.css({'margin-left': -1250*(i)+'px'});
			slider1.fadeIn(500);
		});
		
		li.removeClass('act');
		next.addClass('act');


	};


});